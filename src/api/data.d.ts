export interface ApiResponse<T> {
  code: number;
  msg: string;
  data: T;
}
export type User = {
  id: string;
  username: string;
  password: string;
  name: string;
  number: string;
};
