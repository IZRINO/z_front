import { Axios, AxiosInstance } from "axios";
import request from "../util/request";

export function login<T>(loginData: any) {
  return request<T>({
    url: "/login",
    method: "post",
    params: {
      username: loginData.username,
      password: loginData.password,
    },
  });
}

export function register<T>(ruleForm: any) {
  return request<T>({
    url: "/login/register",
    method: "post",
    data: ruleForm,
  });
}
