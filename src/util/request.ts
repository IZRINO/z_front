import axios from "axios";

const requests = axios.create({
  baseURL: "/api",
  method: "get",
  timeout: 5000,
});

// 请求拦截器
requests.interceptors.request.use((config) => {
  return config;
});

// 响应拦截器
requests.interceptors.response.use(
  (res) => {
    return res;
  },
  (err) => {
    console.log("err" + err);
    return Promise.reject(new Error(err.message || "Error"));
  }
);

export default requests;
