import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../components/HelloWorld.vue";
import SignIn from "../components/SignIn.vue";
import SignUp from "../components/SignUp.vue";
import User from "../components/User.vue";

const routes = [
  { path: "/", redirect: "/home" },
  { path: "/home", component: Home },
  { path: "/signIn", component: SignIn },
  { path: "/signUp", component: SignUp },
  { path: "/user", component: User },
];

const router = createRouter({
  routes,
  history: createWebHashHistory(),
});

export default router;
